<?php

namespace Vulpes\Exception;

class ErrorException extends \ErrorException
{
    public function __toString()
    {
        return <<<STRING
{$this->getSeverityAsString()}: {$this->message}
{$this->getTraceAsStringForToString()}
STRING;
    }

    private function getTraceAsStringForToString()
    {
        return "#0 {$this->file}({$this->line})"
          . substr($traceAsString = $this->getTraceAsString(), strpos($traceAsString, "\n"));
    }

    public function getSeverityAsString()
    {
        switch ($this->severity) {
            case E_ERROR:
                return 'E_ERROR';
            case E_RECOVERABLE_ERROR:
                return 'E_RECOVERABLE_ERROR';
            case E_WARNING:
                return 'E_WARNING';
            case E_PARSE:
                return 'E_PARSE';
            case E_NOTICE:
                return 'E_NOTICE';
            case E_STRICT:
                return 'E_STRICT';
            case E_DEPRECATED:
                return 'E_DEPRECATED';
            case E_CORE_ERROR:
                return 'E_CORE_ERROR';
            case E_CORE_WARNING:
                return 'E_CORE_WARNING';
            case E_COMPILE_ERROR:
                return 'E_COMPILE_ERROR';
            case E_COMPILE_WARNING:
                return 'E_COMPILE_WARNING';
            case E_USER_ERROR:
                return 'E_USER_ERROR';
            case E_USER_WARNING:
                return 'E_USER_WARNING';
            case E_USER_NOTICE:
                return 'E_USER_NOTICE';
            case E_USER_DEPRECATED:
                return 'E_USER_DEPRECATED';
        }
        return get_class($this);
    }
}