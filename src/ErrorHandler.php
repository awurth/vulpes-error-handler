<?php

namespace Vulpes\Error;

use Vulpes\Exception\DeprecatedException;
use Vulpes\Exception\ErrorException;
use Vulpes\Exception\NoticeException;
use Vulpes\Exception\WarningException;
use Throwable;

class ErrorHandler
{
    private ErrorDAOInterface $dao;

    public function __construct(ErrorDAOInterface $dao) { $this->dao = $dao; }

    public function initExceptionHandler(): void
    {
        set_exception_handler([$this, 'handleException']);
    }

    public function initErrorHandler(): void
    {
        set_error_handler([$this, 'handleError']);
    }

    public function handleError(int $severity, string $message, string $file, int $line, array $context)
    {
        $errorExceptionClassName = $this->getClassNameBySeverity($severity);

        /** @var \Vulpes\Exception\ErrorException $exception */
        $exception = new $errorExceptionClassName($message, 0, $severity, $file, $line);

        try {
            $this->dao->save($exception->getSeverityAsString(), $exception);
        } catch (Throwable $throwable) {
            $this->dao->log($exception);
            $this->dao->log($throwable);
        }

        if (!($severity & error_reporting())) {
            return;
        }

        if (ini_get('display_errors')) {
            $this->dao->print($exception);
        }
        exit;
    }

    public function handleException(Throwable $exception)
    {
        try {
            $this->dao->save(get_class($exception), $exception);
        } catch (Throwable $throwable) {
            $this->dao->log($exception);
            $this->dao->log($throwable);
        }
        $this->dao->print($exception);
        exit;
    }

    protected function getClassNameBySeverity(int $severity): string
    {
        switch ($severity) {
            case E_NOTICE:
            case E_USER_NOTICE:
                return NoticeException::class;
            case E_DEPRECATED:
            case E_USER_DEPRECATED:
                return DeprecatedException::class;
            case E_WARNING:
            case E_CORE_WARNING:
            case E_COMPILE_WARNING:
            case E_USER_WARNING:
                return WarningException::class;
        }
        return ErrorException::class;
    }
}