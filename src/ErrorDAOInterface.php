<?php

namespace Vulpes\Error;

use Throwable;

interface ErrorDAOInterface
{
    public function log(Throwable $throwable): void;

    public function print(Throwable $throwable): void;

    public function save(string $type, Throwable $throwable): void;
}